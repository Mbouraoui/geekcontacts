package com.example.mohamed.geekcontacts;

public class Constants {
    public static final String CONTACT_ID_KEY = "contact_id_key";
    public static final String EDITING_KEY = "editing_key";
}
