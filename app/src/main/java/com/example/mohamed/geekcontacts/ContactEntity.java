package com.example.mohamed.geekcontacts;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "contacts")

public class ContactEntity {
    @PrimaryKey(autoGenerate = true)
    int id;

    private String name,gsm,email, linkedinLink, githubLink;

    public ContactEntity(int id, String name, String gsm, String email, String linkedinLink, String githubLink) {
        this.id = id;
        this.name = name;
        this.gsm = gsm;
        this.email = email;
        this.linkedinLink = linkedinLink;
        this.githubLink = githubLink;
    }

    @Ignore
    public ContactEntity() {
        this.name = "";
        this.gsm = "";
        this.email = "";
        this.linkedinLink = "";
        this.githubLink = "";
    }

    @Ignore
    public ContactEntity(String name, String gsm) {
        this.name = name;
        this.gsm = gsm;
        this.email = "";
        this.linkedinLink = "";
        this.githubLink = "";
    }

    @Ignore
    public ContactEntity(String name, String gsm, String email, String linkedinLink, String githubLink) {
        this.name = name;
        this.gsm = gsm;
        this.email = email;
        this.linkedinLink = linkedinLink;
        this.githubLink = githubLink;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getGsm() {
        return gsm;
    }
    public void setGsm(String gsm) {
        this.gsm = gsm;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getLinkedinLink() {
        return linkedinLink;
    }
    public void setLinkedinLink(String linkedinLink) {
        this.linkedinLink = linkedinLink;
    }
    public String getGithubLink() {
        return githubLink;
    }
    public void setGithubLink(String githubLink) {
        this.githubLink = githubLink;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
}
