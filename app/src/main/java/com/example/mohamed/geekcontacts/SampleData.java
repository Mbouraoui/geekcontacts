package com.example.mohamed.geekcontacts;

import java.util.ArrayList;
import java.util.List;

public class SampleData {

    public static List<ContactEntity> getContacts() {
        List<ContactEntity> contacts = new ArrayList<>();
        contacts.add(new ContactEntity("Toto", "123456" ));
        contacts.add(new ContactEntity("Titi", "654321" ));
        return contacts;
    }
}
