package com.example.mohamed.geekcontacts;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditContact extends AppCompatActivity {

    @BindView(R.id.name_text)
    TextView mNameTextView;
    @BindView(R.id.email_text)
    TextView mEmailTextView;
    @BindView(R.id.phone_text)
    TextView mPhoneTextView;
    @BindView(R.id.github_text)
    TextView mGithubTextView;
    @BindView(R.id.linkedin_text)
    TextView mLinkedinTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_check);

        ButterKnife.bind(this);


    }
}
